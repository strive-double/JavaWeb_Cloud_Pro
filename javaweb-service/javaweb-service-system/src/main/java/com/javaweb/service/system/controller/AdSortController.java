// +----------------------------------------------------------------------
// | JavaWeb_Cloud_Pro微服务旗舰版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2019~2020 南京JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <1175401194@qq.com>
// +----------------------------------------------------------------------

package com.javaweb.service.system.controller;


import com.javaweb.common.framework.common.BaseController;
import com.javaweb.common.framework.utils.JsonResult;
import com.javaweb.service.system.entity.AdSort;
import com.javaweb.service.system.query.AdSortQuery;
import com.javaweb.service.system.service.IAdSortService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 广告位管理表 前端控制器
 * </p>
 *
 * @author 鲲鹏
 * @since 2020-11-07
 */
@RestController
@RequestMapping("/adsort")
public class AdSortController extends BaseController {

    @Autowired
    private IAdSortService adSortService;

    /**
     * 获取广告位列表
     *
     * @param adSortQuery 查询条件
     * @return
     */
    @PreAuthorize("@jw.hasPermission('sys:adsort:index')")
    @GetMapping("/index")
    public JsonResult index(AdSortQuery adSortQuery) {
        return adSortService.getList(adSortQuery);
    }

    /**
     * 添加广告位
     *
     * @param entity 实体对象
     * @return
     */
    @PreAuthorize("@jw.hasPermission('sys:adsort:add')")
    @PostMapping("/add")
    public JsonResult add(@RequestBody AdSort entity) {
        return adSortService.edit(entity);
    }

    /**
     * 编辑广告位
     *
     * @param entity 实体对象
     * @return
     */
    @PreAuthorize("@jw.hasPermission('sys:adsort:edit')")
    @PutMapping("/edit")
    public JsonResult edit(@RequestBody AdSort entity) {
        return adSortService.edit(entity);
    }

    /**
     * 删除广告位
     *
     * @param adSortIds 广告位ID
     * @return
     */
    @PreAuthorize("@jw.hasPermission('sys:adsort:delete')")
    @DeleteMapping("/delete/{adSortIds}")
    public JsonResult delete(@PathVariable("adSortIds") Integer[] adSortIds) {
        return adSortService.deleteByIds(adSortIds);
    }

    /**
     * 获取广告位列表
     *
     * @return
     */
    @GetMapping("/getAdSortList")
    public JsonResult getAdSortList() {
        return adSortService.getAdSortList();
    }

}
