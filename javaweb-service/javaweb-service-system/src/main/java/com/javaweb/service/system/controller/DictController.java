// +----------------------------------------------------------------------
// | JavaWeb_Cloud_Pro微服务旗舰版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2019~2020 南京JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <1175401194@qq.com>
// +----------------------------------------------------------------------

package com.javaweb.service.system.controller;


import com.javaweb.common.framework.common.BaseController;
import com.javaweb.common.framework.utils.JsonResult;
import com.javaweb.service.system.entity.Dict;
import com.javaweb.service.system.query.DictQuery;
import com.javaweb.service.system.service.IDictService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 字典表 前端控制器
 * </p>
 *
 * @author 鲲鹏
 * @since 2020-11-01
 */
@RestController
@RequestMapping("/dict")
public class DictController extends BaseController {

    @Autowired
    private IDictService dictService;

    /**
     * 获取字典列表
     *
     * @param dicTypeQuery 查询条件
     * @return
     */
    @PreAuthorize("@jw.hasPermission('sys:dictionary:index')")
    @GetMapping("/index")
    public JsonResult index(DictQuery dicTypeQuery) {
        return dictService.getList(dicTypeQuery);
    }

    /**
     * 添加字典
     *
     * @param entity 实体对象
     * @return
     */
    @PreAuthorize("@jw.hasPermission('sys:dictionary:add')")
    @PostMapping("/add")
    public JsonResult add(@RequestBody Dict entity) {
        return dictService.edit(entity);
    }

    /**
     * 编辑字典
     *
     * @param entity 实体对象
     * @return
     */
    @PreAuthorize("@jw.hasPermission('sys:dictionary:edit')")
    @PutMapping("/edit")
    public JsonResult edit(@RequestBody Dict entity) {
        return dictService.edit(entity);
    }

    /**
     * 删除字典
     *
     * @param dictId 字典ID
     * @return
     */
    @PreAuthorize("@jw.hasPermission('sys:dictionary:delete')")
    @DeleteMapping("/delete/{dictId}")
    public JsonResult delete(@PathVariable("dictId") Integer dictId) {
        return dictService.deleteById(dictId);
    }

}
