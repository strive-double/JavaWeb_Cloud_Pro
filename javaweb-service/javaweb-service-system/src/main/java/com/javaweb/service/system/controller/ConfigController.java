// +----------------------------------------------------------------------
// | JavaWeb_Cloud_Pro微服务旗舰版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2019~2020 南京JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <1175401194@qq.com>
// +----------------------------------------------------------------------

package com.javaweb.service.system.controller;


import com.javaweb.common.framework.common.BaseController;
import com.javaweb.common.framework.utils.JsonResult;
import com.javaweb.service.system.entity.Config;
import com.javaweb.service.system.query.ConfigQuery;
import com.javaweb.service.system.service.IConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 配置表 前端控制器
 * </p>
 *
 * @author 鲲鹏
 * @since 2020-11-06
 */
@RestController
@RequestMapping("/config")
public class ConfigController extends BaseController {

    @Autowired
    private IConfigService configService;

    /**
     * 获取配置列表
     *
     * @param configGroupQuery 查询条件
     * @return
     */
    @PreAuthorize("@jw.hasPermission('sys:configgroup:index')")
    @GetMapping("/index")
    public JsonResult index(ConfigQuery configGroupQuery) {
        return configService.getList(configGroupQuery);
    }

    /**
     * 添加配置
     *
     * @param entity 实体对象
     * @return
     */
    @PreAuthorize("@jw.hasPermission('sys:configgroup:add')")
    @PostMapping("/add")
    public JsonResult add(@RequestBody Config entity) {
        return configService.edit(entity);
    }

    /**
     * 编辑配置
     *
     * @param entity 实体对象
     * @return
     */
    @PreAuthorize("@jw.hasPermission('sys:configgroup:edit')")
    @PutMapping("/edit")
    public JsonResult edit(@RequestBody Config entity) {
        return configService.edit(entity);
    }

    /**
     * 删除配置
     *
     * @param configId 配置ID
     * @return
     */
    @PreAuthorize("@jw.hasPermission('sys:configgroup:delete')")
    @DeleteMapping("/delete/{configId}")
    public JsonResult delete(@PathVariable("configId") Integer configId) {
        return configService.deleteById(configId);
    }

}
