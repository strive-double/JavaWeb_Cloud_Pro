package com.javaweb.service.system.controller;

import com.javaweb.common.framework.common.BaseController;
import com.javaweb.common.framework.utils.JsonResult;
import com.javaweb.service.system.config.CommonConfig;
import com.javaweb.service.system.service.IConfigWebService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * <p>
 * 网站配置 前端控制器
 * </p>
 *
 * @author 鲲鹏
 * @since 2020-11-06
 */
@RestController
@RequestMapping("/configweb")
public class ConfigWebController extends BaseController {

    @Autowired
    private IConfigWebService configWebService;

    /**
     * 获取配置列表
     *
     * @return
     */
    @PreAuthorize("@jw.hasPermission('sys:website:index')")
    @GetMapping("/index")
    public JsonResult index() {
        return configWebService.getList();
    }

    /**
     * 保存配置信息
     *
     * @param info 表单信息
     * @return
     */
    @PreAuthorize("@jw.hasPermission('sys:website:edit')")
    @PutMapping("/edit")
    public JsonResult edit(@RequestBody Map<String, Object> info) {
        if (CommonConfig.appDebug) {
            return JsonResult.error("演示环境禁止操作");
        }
        return configWebService.edit(info);
    }

}
