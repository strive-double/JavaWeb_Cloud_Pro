// +----------------------------------------------------------------------
// | JavaWeb_Cloud_Pro微服务旗舰版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2019~2020 南京JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <1175401194@qq.com>
// +----------------------------------------------------------------------

package com.javaweb.api.system.fallback;

import com.javaweb.api.system.UserClient;
import com.javaweb.api.system.entity.UserInfo;
import com.javaweb.common.framework.utils.JsonResult;
import org.springframework.stereotype.Component;

/**
 * 用户服务调用失败处理
 */
@Component
public class UserClientFallback implements UserClient {

    @Override
    public JsonResult<UserInfo> getInfoByName(String username) {
        return JsonResult.error("用户服务调用失败");
    }
}
